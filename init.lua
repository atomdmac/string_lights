local mod_group = 'string_lights'
local mod_node_names = {
  straight = 'string_lights:straight',
  corner = 'string_lights:corner'
}

function concat_table(t1,t2)
  for k,v in pairs(t2) do
    t1[k] = v
  end
  return t1
end

function determine_dir(pos)
  local east = minetest.get_node({
    x = pos.x + 1,
    y = pos.y,
    z = pos.z
  })

  local west = minetest.get_node({
    x = pos.x - 1,
    y = pos.y,
    z = pos.z
  })

  local north = minetest.get_node({
    x = pos.x,
    y = pos.y,
    z = pos.z + 1
  })

  local south = minetest.get_node({
    x = pos.x,
    y = pos.y,
    z = pos.z - 1,
  })

  local node = mod_node_names.corner
  local dir = nil

  -- Corners
  if minetest.get_item_group(north.name, mod_group) > 0
    and minetest.get_item_group(east.name, mod_group) > 0 then
    dir = 0
    return node, dir
  end

  if minetest.get_item_group(south.name, mod_group) > 0
    and minetest.get_item_group(east.name, mod_group) > 0 then
    dir = 1
    return node, dir
  end

  if minetest.get_item_group(south.name, mod_group) > 0
    and minetest.get_item_group(west.name, mod_group) > 0 then
    dir = 2
    return node, dir
  end

  if minetest.get_item_group(north.name, mod_group) > 0
    and minetest.get_item_group(west.name, mod_group) > 0 then
    dir = 3
    return node, dir
  end

  -- Straight
  node = mod_node_names.straight
  if minetest.get_item_group(north.name, mod_group) > 0
    or minetest.get_item_group(south.name, mod_group) > 0 then
    dir = 1
    return node, dir
  end

  return node, dir
end

local straight, corner


groups = {
  string_lights = 1,
  crumbly = 3,
  oddly_breakable_by_hand = 3,

  -- Support MineClone
  handy = 1,
  _mcl_hardness = 0.6
}

local light_def = {
  name = mod_node_names.straight,
  drawtype = "mesh",
  tiles = { "string_lights.png" },
  mesh = "string_lights_straight.obj",
  paramtype = "light",
  paramtype2 = "facedir",
  param1 = 14,
  light_source = 14,
  walkable = false,
  groups = groups,
  drop = mod_node_names.straight,
  on_place = function (itemstack, placer, node)
    local new_name, dir = determine_dir(node.above)
    itemstack:set_name(new_name)

    return minetest.item_place_node(itemstack, placer, node, dir)
  end
}

straight = light_def
corner = concat_table({}, light_def)
concat_table(
  corner,
  {
    name = mod_node_names.corner,
    mesh = 'string_lights_corner.obj',
    groups = concat_table({
      not_in_creative_inventory = 1
    }, groups)
  }
)

minetest.register_node(mod_node_names.straight, straight)
minetest.register_node(mod_node_names.corner, corner)

local recipe = {
  {'farming:string', 'farming:string', 'farming:string'},
  {'', 'default:torch', ''},
  {'', '', ''}
}

if minetest.get_modpath('mcl_core') then
  recipe = {
    {'mcl_mobitems:string', 'mcl_mobitems:string', 'mcl_mobitems:string'},
    {'', 'mcl_torches:torch', ''},
    {'', '', ''}
  }
end

minetest.register_craft({
  output = mod_node_names.straight .. ' 3',
  recipe = recipe
})

-- Support legacy maps
minetest.register_alias('lights:mesh', mod_node_names.straight)
minetest.register_alias('lights:corner', mod_node_names.corner)
